import Landing from "./components/Landing"
import Vacant from "./components/Vacant"

const routes = [
  {path: '/', name: 'Landing', component: Landing},
  {path: '/Vacant/:id', name: 'Vacant', component: Vacant}
]
/*let routesN=[]
routes.forEach(l=>{
  if(l.children) {
    let r = l.children.map(k => {
      return `${l.path}/${k.path}`
    })
    r.forEach(a=>{
      routesN.push(a)
    })
  }
  else {
    routesN.push(l.path)
  }
})
console.log(routesN)*/
export default routes
